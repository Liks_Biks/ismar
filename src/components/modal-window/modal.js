

import React from 'react'

import Loading from '../loading';
import styled from 'styled-components';
import { Modal } from 'antd'
import Flex from '../../UI/Flex'
import './style.css'
import {useNavigate} from "react-router-dom";

const ModalWindow = (props) => {
	const {
		visible,
		modalContent,
		setVisible,
	} =  props;

	const { email, first_name, last_name, avatar, info} = modalContent
	const navigate = useNavigate()

	const handleCancel = () => {
		navigate("/home")
		setVisible(false)
	}

	return (
		<SModal
			visible={visible}
			onCancel={handleCancel}
			header={false}
			footer={false}
		>
			<Header>
				<span>{first_name}</span> 
				<span> {last_name}</span>
			</Header>
				{
					avatar ?
							<div className="modal-body">
								<div className="modal-content">
									<div className='modal-left'>
										<img src={avatar} alt={first_name} width="150" height="150"/>
										<p>{first_name} {last_name}</p>
										<p><a href={"mailto:" + email}>{email}</a></p>
									</div>
									<div className='modal-right'>
										{info}
									</div>
								</div>
							</div>
					:
						<Loading />
				}
		</SModal>
	)
}

export default ModalWindow

const SModal = styled(Modal)`
	flex-direction:column;
	background: #fff;
	border-radius: 5px;
`
const Header = styled(Flex)`
	width: 100%;
	height: 100%;
`
