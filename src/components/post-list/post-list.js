import React from 'react';
import PostListItem from '../post-list-item';
import './post-list.scss';
import './post-list.css';
import {useNavigate} from "react-router";

const PostList = ({ posts, onDelete, onToggleImportant, onToggleLike}) => {
    const navigate = useNavigate()

    const elem = posts.map((item) => {

        const {id, ...itemProps} = item;

        return (
            <li key={id}>
                <PostListItem
                    {...itemProps}
                        onDelete={()=>onDelete(id)}
                        onToggleImportant={()=>onToggleImportant(id)}
                        onToggleLike={()=>onToggleLike(id)}
                        onOpenWindow={()=> navigate(`/preview/${id}`)}
                        onOpenInfoWindow={() => navigate(`/fullInfo/${id}`)}
                    /> 
            </li>
        );

    });

    return (
        <ul className="app-list list-group">
            {elem}
        </ul>
    )

}

export default PostList;