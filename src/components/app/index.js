import {useNavigate, useParams} from 'react-router-dom';
import App from './app';
import {useLocation} from "react-router";
import React, {useEffect, useState} from "react";
import FetchData from "../../API/fetch-data";
import PostService from "../../API/PostService";

export const HomeApp = () => {
    const [savedUsers, setSavedUsers] = useState([])
    const [visible, setVisible] = useState(false)
    const [modal, setModal] = useState({})

    const params = useParams()
    const location = useLocation()
    const navigate = useNavigate()
    const getdata = new FetchData()

    const saveUser = async data => {
        PostService.saveUser({...data, info: "This is data from db.json"})
        setSavedUsers([...savedUsers, data])
    }

    useEffect(() => {
        if(params.id) {
            const isUserSaved = Boolean(savedUsers.find(item => item.id == params.id))
            if(location.pathname.includes("fullInfo")) {
                const info = "This is data from db.json"
                if(isUserSaved) {
                    const userData = savedUsers.find(item => item.id == params.id)
                    setModal({...userData, info: info})
                }else {
                    getdata.getPostById(params.id)
                        .then(singleData => {
                            setModal({...singleData.data, info: info})
                            saveUser(singleData.data)
                        })
                }
            }else if(location.pathname.includes("preview")) {
                if(isUserSaved) {
                    const userData = savedUsers.find(item => item.id == params.id)
                    setModal(userData)
                }else {
                    getdata.getPostById(params.id)
                        .then(singleData => {
                            setModal({...singleData.data})
                            saveUser(singleData.data)
                        })
                }
            }
            setVisible(true)
        }
    }, [params.id])


    return <App
        savedUsers={savedUsers}
        saveSavedUsers={setSavedUsers}
        modal={modal}
        setModal={setModal}
        visible={visible}
        setVisible={setVisible}
        params={{location: location.pathname, id: params.id, goBack: () => navigate("/home")}}
    />
}