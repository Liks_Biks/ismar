import axios from 'axios'
export const ENDPOINT = "http://localhost:8000"

class PostService { 
    static async saveUser(data) { 
        axios.post(`${ENDPOINT}/users`, data)
    }
    static async getSavedUser(id) {
        const response = await axios.get(`${ENDPOINT}/users/${id}`)
        return response.data
    }
    static async getAllUsersFromDB() {
        const response = await axios.get(`${ENDPOINT}/users`)
        return response.data
    }
}

export default PostService