import React from 'react'
import {HomeApp} from '../components/app/index'
import {Navigate, Route, Routes} from "react-router-dom";

export default function AppRouter() {
    return (
        <Routes>
            <Route 
                path={"/home"}
                element={<HomeApp />}
            />
            <Route 
                path={"/preview/:id"}
                element={<HomeApp />}
            />
             <Route 
                path={"/fullInfo/:id"}
                element={<HomeApp />}
            />
            <Route
                path={"*"}
                element={<Navigate to={"/home"} />}
            />
        </Routes>
    )
}
